package nl.aimsites.nzbvuq.chat;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.chat.interfaces.ISendMessage;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.generators.TeamGenerator;
import nl.aimsites.nzbvuq.game.state.GameState;
import nl.aimsites.nzbvuq.rpc.RPC;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ChatSenderTest {

  public ChatReceiver mChatReceiver;
  private ISendMessage sut;
  private RPC mRPC;
  private Player mPlayer;
  private TeamGenerator mTeamGenerator;

  private Gson gson;
  private String name = "TestName";
  private String chatMessage = "ChatMessage";

  @BeforeEach
  void setup() {
    gson = new Gson();
    mRPC = Mockito.mock(RPC.class);
    mPlayer = Mockito.mock(Player.class);
    mChatReceiver = Mockito.mock(ChatReceiver.class);
    mTeamGenerator = Mockito.mock(TeamGenerator.class);

    sut = new ChatSender(mRPC, mChatReceiver);
  }

  @Test
  void constructorWorks() {
    var expected = sut;

    var actual = new ChatSender(mRPC, mChatReceiver);

    assertEquals(expected.getClass(), actual.getClass());
  }

  @Test
  void sendMessageToGlobal() {
    var nTimesCalled = 1;
    when(mPlayer.getIdentificationKey()).thenReturn("IdentificationKey");
    var players = List.of(mPlayer);
    GameState.getInstance().setOtherPlayers(List.of(mPlayer));
    when(mRPC.sendRequest(any(), any())).thenReturn(null);

    sut.sendMessageToGlobal(name, chatMessage);

    verify(mRPC, times(nTimesCalled)).sendRequest(any(), any());
    verify(mPlayer, times(nTimesCalled)).getIdentificationKey();
  }

  @Test
  void sendMessageToGlobalDoesntSendMessage() {
    var nTimesCalled = 0;
    GameState.getInstance().setOtherPlayers(List.of());
    when(mRPC.sendRequest(any(), any())).thenReturn(null);

    sut.sendMessageToGlobal(name, chatMessage);

    verify(mRPC, times(nTimesCalled)).sendRequest(any(), any());
    verify(mPlayer, times(nTimesCalled)).getIdentificationKey();
  }

  @Test
  void sendMessageToTeam() {
    var nTimesCalled = 1;
    when(mPlayer.getIdentificationKey()).thenReturn("IdentificationKey");
    var players = List.of(mPlayer);
    GameState.getInstance().setTeamGenerator(mTeamGenerator);
    when(mTeamGenerator.getTeam2()).thenReturn(players);
    when(mRPC.sendRequest(any(), any())).thenReturn(null);

    sut.sendMessageToTeam(name, chatMessage);

    verify(mRPC, times(nTimesCalled)).sendRequest(any(), any());
    verify(mPlayer, times(nTimesCalled)).getIdentificationKey();
    assertEquals(GameState.getInstance().getAllPlayersInTeam(), players);
  }

  @Test
  void sendMessageToTeamDoesntSendMessage() {
    var nTimesCalled = 0;
    List<Player> players = List.of();
    GameState.getInstance().setTeamGenerator(mTeamGenerator);
    when(mTeamGenerator.getTeam2()).thenReturn(players);
    when(mRPC.sendRequest(any(), any())).thenReturn(null);

    sut.sendMessageToTeam(name, chatMessage);

    verify(mRPC, times(nTimesCalled)).sendRequest(any(), any());
    verify(mPlayer, times(nTimesCalled)).getIdentificationKey();
    assertEquals(GameState.getInstance().getAllPlayersInTeam(), players);
  }

  @Test
  void updatePlayerList() {
    assertTrue(true);
  }

  @Test
  void sendMessageToPeers() {
    assertTrue(true);
  }
}
