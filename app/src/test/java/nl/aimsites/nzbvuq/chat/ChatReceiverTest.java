package nl.aimsites.nzbvuq.chat;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.chat.dto.Message;
import nl.aimsites.nzbvuq.chat.enums.ChatType;
import nl.aimsites.nzbvuq.chat.interfaces.IDisplayChat;
import nl.aimsites.nzbvuq.chat.interfaces.IReceiveMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class ChatReceiverTest {

  private IReceiveMessage sut;

  private IDisplayChat mIDisplayChat;
  private String playerName = "PlayerName";
  private String messageContent = "Message";
  private Message message = new Message(playerName, messageContent, ChatType.GLOBAL);
  private Gson gson = new Gson();

  @BeforeEach
  void setup() {
    mIDisplayChat = Mockito.mock(IDisplayChat.class);

    sut = new ChatReceiver(mIDisplayChat);
  }

  @Test
  void constructorWorks() {
    var expected = sut;

    var actual = new ChatReceiver(mIDisplayChat);

    assertEquals(actual.getClass(), expected.getClass());
  }

  @Test
  void receiveMessage() {
    var nTimesCalled = 1;

    sut.receiveMessage(List.of(gson.toJson(message)));

    verify(mIDisplayChat, times(nTimesCalled)).displayChatMessage(any(), any(), any());
  }
}
