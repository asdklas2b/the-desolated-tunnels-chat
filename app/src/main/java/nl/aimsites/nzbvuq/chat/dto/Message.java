package nl.aimsites.nzbvuq.chat.dto;

import nl.aimsites.nzbvuq.chat.enums.ChatType;

/**
 * Data object for sending messages.
 *
 * @author Co Ha (cq.ha@student.han.nl)
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 */
public class Message {

  private final String playerName;
  private final String messageContent;
  private final ChatType chatType;

  /**
   * Constructor initialize message.
   *
   * @param playerName Name of player.
   * @param message Message that needs to be sent.
   * @author Co Ha (cq.ha@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public Message(String playerName, String message, ChatType chatType) {
    this.playerName = playerName;
    this.messageContent = message;
    this.chatType = chatType;
  }

  /**
   * Returns name of the player.
   *
   * @return name of player who sent message
   *
   * @author Co Ha (cq.ha@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public String getPlayerName() {
    return playerName;
  }

  /**
   * Returns message.
   *
   * @return content of message
   *
   * @author Co Ha (cq.ha@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public String getMessageContent() {
    return messageContent;
  }

  /**
   * Returns chatType.
   *
   * @return type of message
   * @author Co Ha (cq.ha@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public ChatType getChatType() {
      return chatType;
  }

}
