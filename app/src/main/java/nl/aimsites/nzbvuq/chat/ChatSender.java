package nl.aimsites.nzbvuq.chat;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.chat.dto.Message;
import nl.aimsites.nzbvuq.chat.enums.ChatType;
import nl.aimsites.nzbvuq.chat.interfaces.IDisplayChat;
import nl.aimsites.nzbvuq.chat.interfaces.ISendMessage;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.state.GameState;
import nl.aimsites.nzbvuq.rpc.ConnectionRequestHandler;
import nl.aimsites.nzbvuq.rpc.RPC;
import nl.aimsites.nzbvuq.rpc.message.Request;

import java.util.List;

/**
 * Class is used by game view to send messages to peers.
 *
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 */
public class ChatSender implements ISendMessage {

  private final RPC rpc;
  private static final Gson GSON = new Gson();

  public ChatSender(RPC rpc, IDisplayChat iDisplayChat) {
    this.rpc = rpc;
    ConnectionRequestHandler
            .getInstance()
            .addHandler(new ChatReceiver(iDisplayChat));
  }

  /**
   * Function used to send a message to every player in the gamestate.
   *
   * @param name Name of the player that want to send the chat message.
   * @param chatMessage The chat message that needs to send to the list of peers.
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  @Override
  public void sendMessageToGlobal(String name, String chatMessage) {
    sendMessage(
            GameState.getInstance().getAllPlayersInLobby(),
            "receiveMessage",
            chatMessage,
            name,
            ChatType.GLOBAL
    );
  }

  @Override
  public void sendMessageToPeers(String name, String chatMessage) {
    //
  }

  /**
   * Function used to send a message to everybody in a team. While playing Capture the Flag, for
   * example.
   *
   * @param name Name of the player that want to send the chat message.
   * @param chatMessage The chat message that needs to send to the list of peers.
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  @Override
  public void sendMessageToTeam(String name, String chatMessage) {
    sendMessage(
            GameState.getInstance().getAllPlayersInTeam(),
            "receiveMessage",
            chatMessage,
            name,
            ChatType.TEAM
    );
  }

  @Override
  public void updatePlayerList() {
    //
  }

  /**
   * Function that dynamically sends a message to a list of players.
   *
   * @param players List of players that RPC need to send the message to.
   * @param functionName Name of function that need to be called on remote machine.
   * @param chatMessage Chat message that need to be sent.
   * @param name Name of player that want to send message.
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  private void sendMessage(
          List<Player> players, String functionName, String chatMessage, String name, ChatType chatType) {
    if (!players.isEmpty()) {
      players.forEach(
          player -> {
            rpc.sendRequest(
                player.getIdentificationKey(),
                new Request(
                    ChatReceiver.class.getName(),
                    functionName,
                    List.of(GSON.toJson(new Message(name, chatMessage, chatType)))));
          });
    }
  }
}
