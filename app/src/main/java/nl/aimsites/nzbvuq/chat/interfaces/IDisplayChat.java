package nl.aimsites.nzbvuq.chat.interfaces;


import nl.aimsites.nzbvuq.chat.dto.Message;
import nl.aimsites.nzbvuq.chat.enums.ChatType;

import java.util.List;

/**
 * Class which enables integration between the Chat and View component
 *
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 */
public interface IDisplayChat {
  /**
   * Displays a chat message in the chat box
   *
   * @param chatMessage The chat message that needs to be displayed
   * @param sender sender of the chat message
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  void displayChatMessage(String chatMessage, String sender, ChatType chatType);

  /**
   * Displays the complete chat history
   *
   * @param chatHistory the complete chat history
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  void displayChatHistory(List<Message> chatHistory);
}
