package nl.aimsites.nzbvuq.chat.interfaces;

/**
 * SendMessage interface used to communicate with chat component to send chat messages.
 *
 * @author Co Ha (cq.ha@student.han.nl)
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 */
public interface ISendMessage {
  /**
   * Sends a chat message to a list of peers.
   *
   * @param name Name of the player that want to send the chat message.
   * @param chatMessage The chat message that needs to send to the list of peers.
   * @author Co Ha (cq.ha@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  void sendMessageToPeers(String name, String chatMessage);

  /**
   * Sends a chat message to all team members.
   *
   * @param name Name of the player that want to send the chat message.
   * @param chatMessage The chat message that needs to send to the list of peers.
   * @author Co Ha (cq.ha@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  void sendMessageToTeam(String name, String chatMessage);

  /**
   * Sends a chat message to to all players.
   *
   * @param name Name of the player that want to send the chat message.
   * @param chatMessage The chat message that needs to send to the list of peers.
   * @author Co Ha (cq.ha@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  void sendMessageToGlobal(String name, String chatMessage);

  /**
   * Updates player list.
   *
   * @author Co Ha (cq.ha@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  void updatePlayerList();
}
