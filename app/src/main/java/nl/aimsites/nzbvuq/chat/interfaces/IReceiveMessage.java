package nl.aimsites.nzbvuq.chat.interfaces;

import java.util.List;

/**
 * ReceiveMessage interface used to communicate with chat component handle received messages from
 * other players.
 *
 * @author Co Ha (cq.ha@student.han.nl)
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 */
public interface IReceiveMessage {
  /**
   * Receives messages from other peers.
   *
   * @param message The chat message that has been received.
   * @author Co Ha (cq.ha@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  void receiveMessage(List<String> message);
}
