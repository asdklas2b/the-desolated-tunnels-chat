package nl.aimsites.nzbvuq.chat.enums;

/**
 * Enum for categorizing message types.
 *
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 */
public enum ChatType {
    GLOBAL,
    TEAM,
    PEER
}
