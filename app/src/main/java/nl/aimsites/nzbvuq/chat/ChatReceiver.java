package nl.aimsites.nzbvuq.chat;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.chat.dto.Message;
import nl.aimsites.nzbvuq.chat.interfaces.IDisplayChat;
import nl.aimsites.nzbvuq.chat.interfaces.IReceiveMessage;

import java.util.List;

/**
 * Class used to receive messages sent by nl.aimsites.nzbvuq.chat.ChatSender.
 *
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 */
public class ChatReceiver implements IReceiveMessage {

  private final IDisplayChat iDisplayChat;
  private static final Gson GSON = new Gson();

  public ChatReceiver(IDisplayChat iDisplayChat) {
    this.iDisplayChat = iDisplayChat;
  }

  /**
   * Function used to receive all messages sent by nl.aimsites.nzbvuq.chat.ChatSender.
   *
   * @param payload
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  @Override
  public void receiveMessage(List<String> payload) {
    var message = GSON.fromJson(payload.get(0), Message.class);
      iDisplayChat.displayChatMessage(message.getMessageContent(), message.getPlayerName(), message.getChatType());
  }
}
